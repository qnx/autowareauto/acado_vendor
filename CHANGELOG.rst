^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package acado_vendor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.0 (2021-02-25)
------------------
* Initial port from Autoware.Auto
* Contributors: Joshua Whitley

2.0.0 (2022-04-08)
------------------
* Initial port from Autoware.Auto to QNX
* Contributors: Jordan Mao